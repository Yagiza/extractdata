#include <QDebug>
#include <QCoreApplication>
#include <QFile>
#include <QDir>
#include <QUrl>
#include <QCryptographicHash>

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	if (argc<2)
	{
		qWarning() << "Wrong number of arguments!";
		qWarning() << "Usage:";
		qWarning() << QString("%1 <input_file_name> <output_file_name>").arg(argv[0]);
		return -1;
	}

	QFile file(argv[1]);
	if (!file.open(QIODevice::ReadOnly))
	{
		qWarning() << "Cannot open file for reading!";
		qWarning() << "Error = " << file.error();
		return -1;
	}

	qDebug() << "Reading HTML...";
	QByteArray input(file.readAll());
	file.close();
	qDebug() << "Read! content size=" << input.size();

	file.setFileName(argv[2]);
	if (!file.open(QIODevice::WriteOnly|QIODevice::Truncate))
	{
		qWarning() << "Cannot open file for writing!";
		qWarning() << "Error = " << file.error();
		return -1;
	}

    QString outputDirName = file.fileName().append("_files");
    QDir outputDir;
    if (!outputDir.cd(outputDirName))
    {
        if (!outputDir.mkdir(outputDirName))
        {
            file.close();
            qWarning() << "Cannot create output dir!";
            return -1;
        }
        if (!outputDir.cd(outputDirName))
        {
            file.close();
            qWarning() << "Cannot switch to output dir!";
            return -1;
        }
    }

    qDebug() << "Output dir:" << outputDir.absolutePath();
    QString prefix("src=\"data:image/png;base64,");

	int first=0;
	for (int last=input.indexOf(prefix, first); last!=-1; last=input.indexOf(prefix, first))
	{
		int begin=last+prefix.size();
		last+=5;
		int end=input.indexOf('\"', begin);
		QByteArray src = input.mid(begin, end-begin);
		QByteArray bin = QByteArray::fromBase64(src);
//		qDebug() << "src=" << src;
		QCryptographicHash hash(QCryptographicHash::Sha1);
		hash.addData(bin);
		QString fileName = hash.result().toHex()+".png";
		QFile image(outputDir.filePath(fileName));
		if (image.open(QIODevice::WriteOnly|QIODevice::Truncate))
		{
			image.write(bin);
			image.close();
		}
		else
			qWarning() << "Can't open file for writing:" << fileName;

//		qDebug() << "first/last: [[" << first << "," << last << "]]";
		QByteArray out = input.mid(first, last-first);
//		qDebug() << out;
		file.write(out);
		QUrl url = QUrl::fromLocalFile(image.fileName());
//		qDebug() << "befin/end[[" << begin << "," << end << "]]";
//		qDebug() << url.toEncoded();
		file.write(url.toEncoded());

		first=end;
	}

	QByteArray out = input.mid(first, -1);
//		qDebug() << out;
	file.write(out);
	file.close();

	qDebug() << "Done!";

	return 0;
}
